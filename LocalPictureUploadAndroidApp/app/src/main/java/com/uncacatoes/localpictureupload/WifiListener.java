package com.uncacatoes.localpictureupload;

import android.net.wifi.WifiInfo;

public interface WifiListener {
    public void onWifiUpdate(WifiInfo info);
}
