 package com.uncacatoes.localpictureupload;

import android.content.Context;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

 public class MainActivity extends AppCompatActivity implements WifiListener {

     private WifiInfo info;
     private TextView network_state;
     private String homeBSSID;
     private String homeSSID;
     private TextView home_network;

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        network_state = (TextView)findViewById(R.id.network_state);
        home_network = (TextView)findViewById(R.id.home_network);

        onWifiUpdate(NetworkUtil.getWifiInfo(this));
         Button button = ((Button)findViewById(R.id.setHomeSSID));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WifiInfo _info = NetworkUtil.getWifiInfo(MainActivity.this);
                if(_info!=null && _info.getSupplicantState()== SupplicantState.COMPLETED && _info.getSSID()!= null && !_info.getSSID().isEmpty()){
                    homeBSSID = _info.getBSSID();
                    homeSSID = _info.getSSID();
                    updateHomeSSID();
                    Toast.makeText(MainActivity.this, NetworkUtil.getSSID(MainActivity.this) + " saved as home network", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(MainActivity.this, "Please connect to a wifi network first", Toast.LENGTH_SHORT).show();
                }
            }
        });
        NetWatcher.registerListener(this);
    }

     private void updateNetworkState() {
         if(info.getSSID()!=null) {
             network_state.setText("You are connected to : " + info.getSSID());
         }
         else {
             network_state.setText("You are not connected to a wifi network");
         }
     }

     private void updateHomeSSID(){
         home_network.setText("Saved home network : " + homeSSID);
     }

     @Override
     public void onWifiUpdate(WifiInfo info) {
         this.info = info;
         updateNetworkState();
     }
 }
