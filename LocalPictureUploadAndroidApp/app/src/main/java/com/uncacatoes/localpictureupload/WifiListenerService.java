package com.uncacatoes.localpictureupload;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class WifiListenerService extends IntentService {

    private NotificationManager mNotificationManager;
    private final static String SSID = "\"SFR_37E8\"";


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public WifiListenerService(String name) {
        super(name);
    }

    public WifiListenerService(){
        this(WifiListenerService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_photo_size_select_actual)
                .setContentText("Hello")
                .setContentTitle("World")
                .setOngoing(true)
                .build();
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(123456,notification);
    }

    @Override
    public void onDestroy() {
        if(!isOnLocalNetwork()){
            super.onDestroy();
            mNotificationManager.cancelAll();
        }
    }

    private boolean isOnLocalNetwork() {
        WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        if (wifiManager.isWifiEnabled() && info.getSSID().equals(SSID)) {
            return true;
        }
        return false;
    }
}
