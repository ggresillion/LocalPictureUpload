package com.uncacatoes.localpictureupload;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.util.ArrayList;

import static android.net.ConnectivityManager.TYPE_WIFI;

public class NetWatcher extends BroadcastReceiver {

    private final static String SSID = "\"Cwaartz\"";
    private static ArrayList<WifiListener> listeners = new ArrayList<>();

    @Override
    public void onReceive(Context context, Intent intent) {

        WifiInfo info = NetworkUtil.getWifiInfo(context);
        if(info!=null){
            if (info.getSSID().equals(SSID)) {
                Intent _intent = new Intent(context, WifiListenerService.class);
                context.startService(_intent);
            }
            fireNetworkChange(info);
        }
        else {
            Intent _intent = new Intent(context, WifiListenerService.class);
            context.stopService(_intent);
        }
    }

    private void fireNetworkChange(WifiInfo info) {
        for(WifiListener listener: listeners){
            listener.onWifiUpdate(info);
        }
    }

    public static void registerListener(WifiListener listener){
        listeners.add(listener);
    }
}
